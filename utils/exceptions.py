class BaseException(Exception):

    def __init__(self):
        super(BaseException, self).__init__(self.message)


class EmptyWordDictException(BaseException):
    message = 'word_dict attr must be not empty'
