from collections import defaultdict, OrderedDict
import operator

from utils.exceptions import EmptyWordDictException


class WordCounter:

    def __init__(self, filename):
        self.filename = filename
        self.word_dict = defaultdict(lambda: 0)

    @property
    def _lines(self):
        with open(self.filename, 'r') as f:
            for line in f:
                yield line

    def process(self):
        for line in self._lines:
            self._process_line(line)

        self._sort()

    def __str__(self):
        if not self.word_dict:
            raise EmptyWordDictException

        return '\n'.join(
            '{}:{}'.format(k, v) for k, v in self.word_dict.items()
        )

    def _sort(self):
        if not self.word_dict:
            raise EmptyWordDictException

        sorted_words = sorted(
            self.word_dict.items(),
            key=operator.itemgetter(1, 0),
            reverse=True,
        )

        self.word_dict = OrderedDict(sorted_words)

    def _process_line(self, line):
        for word in line.split():
            word = word.strip(',.!?\r\n').lower()
            self.word_dict[word] += 1
