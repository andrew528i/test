[![pipeline status](https://gitlab.com/andrew528i/test/badges/dev/pipeline.svg)](https://gitlab.com/andrew528i/test/commits/dev) [![coverage report](https://gitlab.com/andrew528i/test/badges/dev/coverage.svg)](https://gitlab.com/andrew528i/test/commits/dev)

# Local testing

```bash
pip install -r requirements.txt
```

# Running
`Notice`: before running script don't forget to create a text file.

```bash
python run.py
```

# Output example
```
Please type in text filename: file.txt
the:14
a:9
of:8
occurrences:5
must:5
is:5
with:4
script:4
number:4
in:4
be:4
words:3
we:3
to:3
that:3
task:3
python:3
and:3
word:2
will:2
their:2
text:2
sorted:2
result:2
program:2
output:2
out:2
order:2
line:2
it:2
from:2
complete:2
candidate:2
as:2
very:1
uses:1
us:1
unique:1
understand:1
this:1
think:1
than:1
test:1
take:1
statistics:1
stage:1
solution:1
skills:1
simple:1
screen:1
reading:1
read:1
programming:1
programmer:1
professional:1
printed:1
print:1
pre-interview:1
operability:1
on:1
no:1
new:1
more:1
level:1
its:1
interface:1
intended:1
input:1
hour:1
help:1
format:1
file:1
equal:1
each:1
don't:1
developed:1
desirable:1
descending:1
command:1
checking:1
carefully:1
candidates:1
can:1
by:1
better:1
believe:1
at:1
assignment:1
apart:1
an:1
also:1
alphabetic:1
all:1
3:1
```
