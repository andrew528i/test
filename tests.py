from unittest import TestCase
from datetime import datetime

from mock import patch, mock_open, call

from utils import WordCounter
from utils.exceptions import EmptyWordDictException


class WordCounterTestCase(TestCase):

    _line = 'Hello, ?World.!\n'

    @property
    def radom_filename(self):
        return 'test.{}'.format(datetime.now().timestamp())

    def test_init(self):
        filename = self.radom_filename
        wc = WordCounter(filename)

        self.assertEqual(wc.filename, filename)

    def test__lines_without_file(self):
        filename = filename = self.radom_filename
        wc = WordCounter(filename)

        with self.assertRaises(FileNotFoundError):
            next(wc._lines)

    def test__lines_with_file(self):
        mo = mock_open()
        filename = filename = self.radom_filename
        wc = WordCounter(filename)

        with patch('builtins.open', mo):
            lines = [line for line in wc._lines]

        self.assertIn(
            call(filename, 'r'),
            mo.mock_calls,
        )

    def test__process_line(self):
        filename = filename = self.radom_filename
        wc = WordCounter(filename)

        wc._process_line(self._line)

        self.assertEqual(len(wc.word_dict), 2)
        self.assertEqual(wc.word_dict.get('hello'), 1)
        self.assertEqual(wc.word_dict.get('world'), 1)

    def test___str__(self):
        filename = filename = self.radom_filename
        wc = WordCounter(filename)

        with self.assertRaises(EmptyWordDictException):
            str(wc)

        wc._process_line(self._line)
        self.assertEqual(str(wc), 'hello:1\nworld:1')
