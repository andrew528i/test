import os

from utils import WordCounter


if __name__ == '__main__':
    text_filename = input('Please type in text filename: ')

    try:
        wc = WordCounter(text_filename)
        wc.process()
        print(wc)
    except FileNotFoundError:
        print('File does not exists: {}'.format(text_filename))
